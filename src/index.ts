type debounceActionCallback = () => void;

export default function debounceAction(callback: debounceActionCallback, timeout: number = 500) {
  let timerId: number;
  return function (...args) {
    clearTimeout(timerId);
    timerId = setTimeout(() => {
      callback.call(this, ...args)
    }, timeout)
  }
}
